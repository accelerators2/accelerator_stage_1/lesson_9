import 'package:flutter/material.dart';
import 'package:flutter_app_1/constants/app_assets.dart';
import 'package:flutter_app_1/constants/app_colors.dart';
import 'package:flutter_app_1/ui/locations/locations_list/locations_list_screen.dart';
import 'package:flutter_app_1/ui/persons_list/persons_list_screen.dart';
import 'package:flutter_svg/svg.dart';

import '../generated/l10n.dart';
import '../ui/settings_screen.dart';

class AppNavBar extends StatelessWidget {
  const AppNavBar({
    Key? key,
    required this.current,
  }) : super(key: key);

  final int current;

  PageRouteBuilder _createRoute(Widget screen) {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) => screen,
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        return child;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: const BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: AppColors.neutral3,
            blurRadius: 3.0,
            offset: Offset(0, -3),
          )
        ],
      ),
      child: BottomNavigationBar(
        elevation: 0,
        currentIndex: current,
        selectedItemColor: AppColors.primary,
        unselectedItemColor: AppColors.neutral3,
        selectedFontSize: 14.0,
        unselectedFontSize: 14.0,
        items: [
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              AppAssets.svg.persons,
              color: current == 0 ? AppColors.more1 : AppColors.neutral3,
            ),
            label: S.of(context).persons,
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              AppAssets.svg.locations,
              color: current == 1 ? AppColors.more1 : AppColors.neutral3,
            ),
            label: S.of(context).locations,
          ),
          BottomNavigationBarItem(
            icon: const Icon(Icons.settings_outlined),
            label: S.of(context).settings,
            activeIcon: const Icon(
              Icons.settings_outlined,
              color: AppColors.more1,
            ),
          ),
        ],
        onTap: (index) {
          if (index == 0) {
            Navigator.of(context).pushAndRemoveUntil(
              _createRoute(const PersonsListScreen()),
              (route) => false,
            );
          } else if (index == 1) {
            Navigator.of(context).pushAndRemoveUntil(
              _createRoute(const LocationsListScreen()),
              (route) => false,
            );
          } else if (index == 2) {
            Navigator.of(context).pushAndRemoveUntil(
              _createRoute(const SettingsScreen()),
              (route) => false,
            );
          }
        },
      ),
    );
  }
}
