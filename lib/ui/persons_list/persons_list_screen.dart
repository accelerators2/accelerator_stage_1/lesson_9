import 'package:flutter/material.dart';
import 'package:flutter_app_1/bloc/persons/bloc_persons.dart';
import 'package:flutter_app_1/constants/app_colors.dart';
import 'package:flutter_app_1/constants/app_styles.dart';
import 'package:flutter_app_1/dto/person.dart';
import 'package:flutter_app_1/generated/l10n.dart';
import 'package:flutter_app_1/widgets/app_nav_bar.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/persons/states.dart';
import 'widgets/person_grid_tile.dart';
import 'widgets/person_list_tile.dart';
import 'widgets/search_field.dart';

part 'widgets/_grid_view.dart';
part 'widgets/_list_view.dart';

class PersonsListScreen extends StatelessWidget {
  const PersonsListScreen({Key? key}) : super(key: key);

  static final isListView = ValueNotifier(true);

  @override
  Widget build(BuildContext context) {
    return ColoredBox(
      color: Theme.of(context).scaffoldBackgroundColor,
      child: SafeArea(
        child: Scaffold(
          bottomNavigationBar: const AppNavBar(current: 0),
          body: Column(
            children: [
              SearchField(
                onChanged: (value) {
                  BlocProvider.of<BlocPersons>(context).add(
                    EventPersonsFilterByName(value),
                  );
                },
              ),
              BlocBuilder<BlocPersons, StateBlocPersons>(
                builder: (context, state) {
                  var personsTotal = 0;
                  if (state is StatePersonsData) {
                    personsTotal = state.data.length;
                  }
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 12.0),
                    child: Row(
                      children: [
                        Expanded(
                          child: Text(
                            S.of(context).personsTotal(personsTotal).toUpperCase(),
                            style: AppStyles.s10w500.copyWith(
                              letterSpacing: 1.5,
                              color: AppColors.neutral2,
                            ),
                          ),
                        ),
                        IconButton(
                          icon: const Icon(Icons.grid_view),
                          iconSize: 28.0,
                          color: AppColors.neutral2,
                          onPressed: () {
                            isListView.value = !isListView.value;
                          },
                        ),
                      ],
                    ),
                  );
                },
              ),
              Expanded(
                child: BlocBuilder<BlocPersons, StateBlocPersons>(
                  builder: (context, state) {
                    return state.when(
                      initial: () => const SizedBox.shrink(),
                      loading: () => Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const [
                          CircularProgressIndicator(),
                        ],
                      ),
                      data: (data) {
                        if (data.isEmpty) {
                          return Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Flexible(
                                child: Text(S.of(context).personsListIsEmpty),
                              ),
                            ],
                          );
                        } else {
                          return ValueListenableBuilder<bool>(
                            valueListenable: isListView,
                            builder: (context, isListViewMode, _) {
                              return isListViewMode ? _ListView(personsList: data) : _GridView(personsList: data);
                            },
                          );
                        }
                      },
                      error: (error) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Flexible(
                              child: Text(error),
                            ),
                          ],
                        );
                      },
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
