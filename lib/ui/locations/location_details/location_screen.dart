import 'package:flutter/material.dart';
import 'package:flutter_app_1/constants/app_colors.dart';
import 'package:flutter_app_1/constants/app_styles.dart';
import 'package:flutter_app_1/dto/location.dart';
import 'package:flutter_app_1/generated/l10n.dart';
import 'package:intl/intl.dart';

class LocationScreen extends StatelessWidget {
  const LocationScreen({Key? key, required this.location}) : super(key: key);
  final Location location;
  String get info {
    return [
      location.type ?? S.current.noData,
      '·',
      location.dimension ?? S.current.noData,
    ].join(' ');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        iconTheme: const IconThemeData(color: Colors.black),
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Flexible(
                child: Text(
                  location.name ?? S.of(context).noData,
                  style: AppStyles.s24w700,
                ),
              ),
            ],
          ),
          const SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Flexible(
                child: Text(
                  info,
                  style: AppStyles.s16w400.copyWith(
                    color: AppColors.neutral2,
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(height: 20),
          Row(
            children: [
              Expanded(
                child: Text(
                  '${S.of(context).aired}: '
                  '${DateFormat('EEEE dd MMMM yyyy').format(location.created!)}',
                  style: AppStyles.s16w400,
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
