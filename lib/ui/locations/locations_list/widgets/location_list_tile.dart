import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_1/constants/app_colors.dart';
import 'package:flutter_app_1/constants/app_styles.dart';
import 'package:flutter_app_1/dto/location.dart';
import 'package:flutter_app_1/generated/l10n.dart';
import 'package:flutter_app_1/ui/locations/location_details/location_screen.dart';

class LocationListTile extends StatelessWidget {
  const LocationListTile(this.location, {Key? key}) : super(key: key);

  final Location location;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Row(
        children: [
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Text(
                        location.type ?? S.of(context).noData,
                        style: AppStyles.s12w500.copyWith(
                          letterSpacing: 1.5,
                          color: AppColors.neutral2,
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Expanded(
                      child: Text(
                        location.name ?? S.of(context).noData,
                        style: AppStyles.s16w500.copyWith(
                          height: 1.6,
                          leadingDistribution: TextLeadingDistribution.even,
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Expanded(
                      child: Text(
                        location.dimension ?? S.of(context).noData,
                        style: const TextStyle(
                          color: AppColors.neutral2,
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
          Row(
            children: const [
              Icon(Icons.chevron_right),
            ],
          ),
        ],
      ),
      onTap: () {
        Navigator.of(context).push(
          CupertinoPageRoute(
            builder: (context) => LocationScreen(location: location),
          ),
        );
      },
    );
  }
}
